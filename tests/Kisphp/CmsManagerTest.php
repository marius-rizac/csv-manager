<?php

namespace Tests\Kisphp;

use Kisphp\CsvManager;
use PHPUnit\Framework\TestCase;

class CmsManagerTest extends TestCase
{
    public function test_createCsv()
    {
        $data = $this->getCsvDataArray();

        $csv = new CsvManager();

        $filePath = $this->getFilePath();

        $csv->createCsv($filePath, $data);

        $content = "user,email
alfa,alfa@example.com
beta,beta@example.com
";

        self::assertSame($content, file_get_contents($filePath));
    }

    /**
     * @depends test_createCsv
     */
    public function test_readCsv()
    {
        $csv = new CsvManager();
        $filePath = $this->getFilePath();

        $content = $csv->readCsv($filePath);
        unlink($this->getFilePath());

        self::assertSame($this->getCsvDataArray(), $content);
    }

    /**
     * @return array
     */
    protected function getCsvDataArray(): array
    {
        $data = [
            [
                'user' => 'alfa',
                'email' => 'alfa@example.com',
            ],
            [
                'user' => 'beta',
                'email' => 'beta@example.com',
            ],
        ];

        return $data;
    }

    /**
     * @return string
     */
    protected function getFilePath(): string
    {
        return __DIR__ . '/csv-demo.csv';
    }
}
