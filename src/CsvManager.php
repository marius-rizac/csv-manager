<?php

namespace Kisphp;

class CsvManager
{
    /**
     * @var int
     */
    protected $length;

    /**
     * @var string
     */
    protected $delimiter;

    /**
     * @param int $length
     * @param string $delimiter
     */
    public function __construct(int $length= 1000, string $delimiter = ',')
    {
        $this->length = $length;
        $this->delimiter = $delimiter;
    }

    /**
     * @param string $filePath
     * @param array $data
     */
    public function createCsv($filePath, array $data)
    {
        $fp = fopen($filePath, 'w');

        if (isset($data[0])) {
            fputcsv($fp, array_keys($data[0]));
        }

        foreach ($data as $fields) {
            fputcsv($fp, $fields);
        }

        fclose($fp);
    }

    /**
     * @param string $filePath
     *
     * @return array
     */
    public function readCsv($filePath)
    {
        $handle = fopen($filePath, 'r');
        if ($handle === false) {
            return [];
        }

        $isFirst = true;
        $content = [];
        while (($data = fgetcsv($handle, $this->length, $this->delimiter)) !== false) {
            if ($isFirst === true) {
                $keys = $data;
                $isFirst = false;
                continue;
            }

            $item = [];
            foreach ($data as $key => $val) {
                $item[$keys[$key]] = $val;
            }
            $content[] = $item;
            unset($item);
        }
        fclose($handle);

        return $content;
    }
}
